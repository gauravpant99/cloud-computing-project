# README #


### Movie Recommendation ###

Recommendation systems changed the way inanimate websites communicate with their users. Rather than providing a static experience in which users search for and potentially buy products, recommender systems increase interaction to provide a richer experience. Recommender systems identify recommendations autonomously for individual users based on past purchases and searches, and on other users' behavior. The movie recommendation systems help
in predicting the choice of movie for the users based on the interests and the historical data and it is one of the most popular application of big data processing.

##Algorithms Implemented:
###1 Alternating Least Squares is a method that alternates between two matrices in a product such as Y=UV′Y=UV′ where Y is data. 
In this case we have u and v is user and movie respectively. Essentially it guesses U to estimate V and then alternates back and forth until U and V stop changing. By fixing one or the other it becomes a simple least squares solution (by generalized inverses). We will be fixing first the movie matrix and then the user matrix for ALS

###2 Cosine similarity This model is a vector-space approach based on linear algebra rather than a statistical approach. 
Movies are represented as |I|-dimensional vectors and similarity is measured by the cosine distance between two movie vectors. 
This can be computed efficiently by taking their dot product and dividing it by the product of their L2 (Euclidean) norms:


* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

###Steps to Execute:
* Copy run_als.py and parse.py to the location /user/<home_directory> in the server
* mkdir -p /user/gpant/spark/input/recommendation/
* Parse the movie lens file using the parse.py script.
* This script needs 2 argument. The input will be the ratings.csv which is of the below format in csv (available in input directory).
<user_id>,<movie_id>,<rating>,<timestamp>
* Issue the below command to make a csv file in the format where each row corresponds to the user and
  columns correspond to the movies. The dimension will be u*m. Movies which are not rated by user are initialized with 0  value.
*User1: <movie1_rating>,<movie2_rating>,<movie3_rating>
*User2: <movie1_rating>,<movie2_rating>,<movie3_rating>

* python parse.py ratings.csv  parse_ratings.csv
* Copy ratings.csv from the InputFiles directory .
	*hadoop fs -put parse_ratings.csv /user/gpant/spark/input/recommendation/
	*Issue the below command to execute the spark process.  
	
		spark-submit run_als.py /user/gpant/spark/input/recommendation/parse_ratings.csv  
		
	*The output will be printed in the console with the user and the ratings. 
	Also, a json file will be stored in the working directory as well(sample given in output folder which will be in json format).
*Below is a snippet 
	Movie prediction for user 1: for movie_id 215: Predicted rating 2  
	
	Movie prediction for user 2: for movie_id 100: Predicted rating 3  
	
	Movie prediction for user 3: for movie_id 403: Predicted rating 1  
	
	Movie prediction for user 4: for movie_id 436: Predicted rating 2  
	
	Movie prediction for user 5: for movie_id 100: Predicted rating 1  
	
	Movie prediction for user 6: for movie_id 122: Predicted rating 2  
	
	Movie prediction for user 7: for movie_id 50: Predicted rating 2  
	


