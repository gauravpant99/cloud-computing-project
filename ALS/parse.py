## Parse Script to integrate the value of ratings.csv
## into a csv file where each row represents the
## user and each column represents the movie
## The output of this will be feeded to the
## run_als script.
## Run using python parse.py <input_file_name>


import sys
import csv
movie_for_each_user={}
all_users=[]
all_movies=[]
with open(sys.argv[1], "r") as ins:
    array=[]
    for line in ins:
       each_row_array=line.split(',')
       movie= each_row_array[1]
       user=  each_row_array[0]
       rating=each_row_array[2]
       if not int(movie) in all_movies:
          all_movies.append(int(movie))
       if not int(user) in movie_for_each_user:
          all_users.append(int(user))
          movie_for_each_user[int(user)]={}
          movie_for_each_user[int(user)][int(movie)]=float(rating)
       else:
          movie_for_each_user[int(user)][int(movie)]=float(rating)


b = open(sys.argv[2], 'w')
a = csv.writer(b)

## {1: {}}
print ('Total Movies: %d' %len(all_movies))
print ('Total Users: %d' %len(all_users))

for user in all_users:
    users=[0.0]*len(all_movies)
    for movie in range(0, len(all_movies)):
        if user in movie_for_each_user:
           if all_movies[movie] in movie_for_each_user[user]:
              try:
                  movie_exist=all_movies[movie]
                  users[movie]=float(movie_for_each_user[user][movie_exist])
              except: 
                  continue;
    a.writerows([users])
b.close() 
