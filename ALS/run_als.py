'''
Cloud Project ALS implementation
Group 2
Team: Gaurav P, Pawan A, Vinayaka N
'''
from pyspark import SparkContext
import numpy as np
import json
from numpy.random import rand
from numpy import matrix
import sys
import timeit;


#ALS require to keep one matrix fixed
#Fixed the user matrix fix
def keep_user_fix(x):
    user = broadc_users.value.T * broadc_users.value
    for a in range(features):
        user[a, a] = user[a,a] + value_lam * n
    new_user = broadc_users.value.T * all_user_rating.value[x,:].T
    return np.linalg.solve(user, new_user)

#function to calculate RMSE of movie and user    
#The goal is to reduce RMSE after iterations and
#check if it has converged
def find_rmse(each_user_rating, movies, users):
    mu = movies * users.T
    differ_value = each_user_rating - mu
    s_diff_value = (np.power(differ_value, 2)) / (movies_row * users_row)
    return np.sqrt(np.sum(s_diff_value))

#ALS require to keep one matrix fixed
#Fixing the movie fixed to update
def keep_movie_fix(x):
    u_rating_t = all_user_rating.value.T
    #Multiply with transpose value 
    movie = movies_feat.value.T * movies_feat.value
    for i in range(features):
        movie[i, i] = movie[i,i] + value_lam * m
    movie_T=movies_feat.value.T    
    rating_T=u_rating_t[x, :].T
    updated_movie = movie_T * rating_T
    #solve using linear algebra
    solve_eq=np.linalg.solve(movie, updated_movie)
    return solve_eq

##Program executes from here
##Usage: python run_als.py <input_file_path>
   
if __name__ == "__main__":
    value_lam = 0.001
    total_iterations =  10
    features = 10
    rms = np.zeros(total_iterations)
    obj =[]  #Store the value for json

##Initialize a spark Context
    sc = SparkContext(appName="run_als")
    start = timeit.default_timer()
   
# Loading the movielens data in to the matrix
    lines = sc.textFile(sys.argv[1])
    each_line = lines.map(lambda line: line.split(","))
    each_user_rating = np.matrix(each_line.collect()).astype('float')
    m,n = each_user_rating.shape
    
    all_user_rating = sc.broadcast(each_user_rating)
# Initializing the weights matrix using the user_rating matrix
    weights = np.zeros(shape=(m,n))
    for r in range(m):
        for j in range(n):
            if each_user_rating[r,j]>0.5:
                weights[r,j] = 1.0
            else:
                weights[r,j] = 0.0
      
# Randomly initialize movies and users matrix
    movies = matrix(rand(m, features))
    movies_feat = sc.broadcast(movies)
    
    users = matrix(rand(n, features))
    broadc_users = sc.broadcast(users)
   
    ##Get dimension of the matrix 
    movies_row,movies_col = movies.shape
    users_row,users_col = users.shape

# iterate until movies and users matrix converge
    for i in range(0,total_iterations):
    #Fixing the user matrix to find the movie matrix. 
        movies = sc.parallelize(range(movies_row)).map(keep_user_fix).collect()
        movies_feat = sc.broadcast(matrix(np.array(movies)[:, :]))

    # solving user matrix by keeping movie matrix constant 
        users = sc.parallelize(range(users_row)).map(keep_movie_fix).collect()
        broadc_users = sc.broadcast(matrix(np.array(users)[:, :]))

        calculated_error = find_rmse(each_user_rating, matrix(np.array(movies)), matrix(np.array(users)))
        rms[i] = calculated_error
    fe_user = np.array(users).squeeze()
    fe_movie = np.array(movies).squeeze()
    
    dot_final = np.dot(fe_movie,fe_user.T)
    
# subtraction of  the rating that user already rated
    max_rating=5
    ##Across axisi 1 do the argmax
    recommended_movie = np.argmax(dot_final - max_rating * weights,axis =1)
    
# Predicting movies for each users
    for u in range(0, movies_row):
        r = recommended_movie.item(u)
        p = dot_final.item(u,r)
	obj.append({"user": u+1,"movie":r,"p":p})	    
       
        print ('Movie prediction for user %d: for movie_id %d: Predicted rating %f' %(u+1,r+1,p) )
        
    print "RMSE after each iterations: ",rms
    ##Creating a temp file for storing the json format
    outputfile = open("jsonoutput.json","w")
    json.dump(obj,outputfile,indent=4)
    outputfile.close()
    stop = timeit.default_timer()
    print "------ Time taken ---",stop - start
    print "Avg rmse---- ",np.mean(rms)
    sc.stop()
