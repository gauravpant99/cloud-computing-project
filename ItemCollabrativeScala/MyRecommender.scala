package org.apache.spark.examples

/**
  * @author : Vinayaka Narayan , Gaurav Pant , Pawan Araballi
  */
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions


object MyRecommender 
{
  var simType: String = "cosine";
  var numberOfNeighbors: Int = _
  var numberOfReco: Int = _
  var ipFile: String = _
  var opFile: String = _
  var ipUri: String = _
  var opUri: String = _
  var topItemSimilarities: Array[(Int, Array[(Int, Double)])] = _

  /**
    * Input Arguments to the program from the user
     * args[1] numberOfNeighbors
    * args[2] numberOfRecommendations
    * args[3] inuput file Path Uri
    * args[4] output file Path Uri
    */

  //Main function0 to calculate running time
  def main(args: Array[String])
  {
    val t0 = System.nanoTime();
    numberOfNeighbors = args(0).toInt
    numberOfReco = args(1).toInt
    ipFile = args(2)
    opFile = args(3)
    val conf = new SparkConf()
      .setMaster("local")
      .setAppName("MyRecommender")
    val sc = new SparkContext(conf)
    val userBasedRatings = sc.textFile(ipFile).map { line =>
      val fields = line.split("\t")
      (fields(0).toInt, (fields(1).toInt, fields(2).toDouble))
    }
    //user id | item id | rating
    userBasedRatings.foreach{ i =>println("This is the rating tuple = " + i )}

    val recommendations = getRecommendations(userBasedRatings)

    recommendations.foreach(x => println("Output tuple is " + x))

    recommendations.foreach(println)
    recommendations.foreach{x => x; print(x._1 + " " + x._2 +  " " + x._3) }
    recommendations.saveAsTextFile(opFile)
    val t1 = System.nanoTime();
    println("Time Taken: " + (t1-t0) + " ns");
  }


  /**
    * Finds similarity between all item pairs by using a similarity measure on the common ratings given by users to the items
    * [(ratingByUserX,ratingByUserY)] <- Common user ratings for itemPair
    * ((item1,item2),similarity)
    */
  def calculateSimilarity(itemPair: (Int, Int), userRatingPairs: Iterable[(Double, Double)], simType: String): ((Int, Int), Double) = {
    var sumXX, sumYY, sumXY, sumX, sumY, sumOfSquares = 0.0
    var n = 0

    for (userRatingPair <- userRatingPairs) {
      sumX += userRatingPair._1
      sumY += userRatingPair._2
      sumXX += Math.pow(userRatingPair._1, 2)
      sumYY += Math.pow(userRatingPair._2, 2)
      sumXY += userRatingPair._1 * userRatingPair._2
      sumOfSquares += Math.pow(userRatingPair._1
        - userRatingPair._2, 2);
      n += 1
    }
    var similarity = calculate_cosine(sumXX, sumYY, sumXY)
    return (itemPair, similarity)
  }


  /**
    * This function Calculates the Cosine similarity.
    * Cosine similarity is a measure of similarity between two non zero vectors(Movie item vectors)
    * of an inner product space that measures
    * the cosine of the angle between them. The cosine of 0° is 1, and it is less than 1 for any other angle.
    * More similar items will have a value close to 1 and dissimilar value can be negative too
    * sine cosine funtion oscillates from -1,0,1
    * */
  def calculate_cosine(sumXX: Double, sumYY: Double, sumXY: Double): Double = {
    val numerator = sumXY
    val denominator = math.sqrt(sumXX) * math.sqrt(sumYY)
    if (denominator != 0) {
      return (numerator / denominator)
    }
    return 0.0
  }


  /**
    * Convert from ((item1,item2),similarity) to (item1,(item2,similarity)) and (item2,(item1,similarity))
    */
  def keyOnFirstItem(itemPair: (Int, Int), similarity: Double): List[(Int, (Int, Double))] = {
    return List((itemPair._1, (itemPair._2, similarity)), (itemPair._2, (itemPair._1, similarity)))
  }

  /**
    * For every item finds its N most similar items
    * KNN will be applied to find the closest
    * neighour.
    */
  def nearestNeighbors(item: Int, otherItemSimilarities: Array[(Int, Double)], n: Int): (Int, Array[(Int, Double)]) = {
    return (item, otherItemSimilarities.sortBy(c => c._2)(Ordering[Double].reverse).take(n))
  }

  /**
    * For a user finds top N recommended items. This is based on the input arguments
    */
  def topNRecommendations(userId: Int, itemRatings: Map[Int, Double], topItemSimilarities: Map[Int, Array[(Int, Double)]], n: Int): ListBuffer[(Int, Int, Double)] = {
    val scores = HashMap.empty[Int, Double]
    val totalSim = HashMap.empty[Int, Double]
    for (similarity <- topItemSimilarities) {
      scores put (similarity._1, 0.0)
      totalSim put (similarity._1, 0.0)
    }
    // For every non rated item aggregate similary*rating and similarity
    for (itemRating <- itemRatings) {
      val similarities = topItemSimilarities(itemRating._1)
      for (similarity <- similarities) {
        if (!itemRatings.contains(similarity._1)) {
          scores(similarity._1) += (similarity._2 * itemRating._2)
          totalSim(similarity._1) += (similarity._2)
        }
      }
    }
    // Normalize and take top N predictions
    val predictions = ListBuffer.empty[(Int, Int, Double)]
    val rankings = topItemSimilarities.toArray.map(x => (x._1, scores(x._1) / totalSim(x._1))).filter(x => !x._2.isNaN())
    for (ranking <- rankings.sortBy(c => c._2)(Ordering[Double].reverse).take(n)) {
      predictions.append((userId, ranking._1, ranking._2))
    }
    return predictions
  }


  /**
    * Calls other methods
    * This function is called by the fileReco
    * Input to this function is
    * This is a sample input to this function =  (user id | ( item id | rating ) ) ie (1,(1,3.0))
    * Output of this function is the recommendation
    * This is sample output of the function = (user id | Recommended movie id | Predicted rating) is (1,3,3.0)
    */
    def getRecommendations(userBasedRatings: RDD[(Int, (Int, Double))]): RDD[(Int, Int, Double)] = {

    val groupedUserBasedRatings = userBasedRatings.groupByKey().map(x => (x._1, x._2.toArray.sortBy(c => c._1)))
    //groupedUserBasedRatings.foreach{x => x._2; println(x._2.foreach(println))}

    val itemPairsWithCommonUserRatings = groupedUserBasedRatings.flatMap(x => x._2.toArray.combinations(2).toArray.map(x => ((x(0)._1, x(1)._1), (x(0)._2, x(1)._2)))).groupByKey()
    //itemPairsWithCommonUserRatings.foreach(println)
    //itemPairsWithCommonUserRatings.foreach{x => x._2; println(x._2.foreach(println))}

    val itemSimilarities = itemPairsWithCommonUserRatings.map(p => calculateSimilarity(p._1, p._2, simType)).flatMap(p => keyOnFirstItem(p._1, p._2)).groupByKey()
    //itemSimilarities.foreach(println)
    topItemSimilarities = itemSimilarities.map(p => nearestNeighbors(p._1, p._2.toArray, numberOfNeighbors)).collect()

    val recommendations = groupedUserBasedRatings.flatMap(x => topNRecommendations(x._1, x._2.toMap, topItemSimilarities.toMap, numberOfReco))
    //recommendations.foreach(println)
    return recommendations;
  }

  
  
}